package service

import (
	"os"
	"time"
)

type _closeEvent int

const (
	_serviceCloseEvent _closeEvent = 1 << iota
	_timeOutCloseEvent
	_mandatoryCloseEvent
)

func (c _closeEvent) String() string {
	switch c {
	case _serviceCloseEvent:
		return "服务异常退出关闭"
	case _timeOutCloseEvent:
		return "按了一次ctrl+c后 超时关闭"
	case _mandatoryCloseEvent:
		return "按了一次ctrl+c 在超时时间内在按一次ctrl+c"
	}
	return "其他"
}

type closeResource struct {
	signalRecord map[os.Signal]int
	errList      []error
}

var _closeManage = map[_closeEvent]Closer{
	_serviceCloseEvent:   &_serviceStopHandle{},
	_timeOutCloseEvent:   &_timeoutStopHandle{},
	_mandatoryCloseEvent: &_mandatoryStopHandle{},
}

type Closer interface {
	IsCloseAllService(resource closeResource) bool
}

type _serviceStopHandle struct{}

func (s *_serviceStopHandle) IsCloseAllService(resource closeResource) bool {
	if len(resource.errList) > 0 {
		return true
	}
	return false
}

type _timeoutStopHandle struct{}

func (t *_timeoutStopHandle) IsCloseAllService(resource closeResource) bool {
	if _, ok := resource.signalRecord[os.Interrupt]; ok {
		time.Sleep(10 * time.Second)
		return true
	}
	return false
}

type _mandatoryStopHandle struct{}

func (t *_mandatoryStopHandle) IsCloseAllService(resource closeResource) bool {
	if v, ok := resource.signalRecord[os.Interrupt]; ok && v >= 2 {
		return true
	}
	return false
}
