package service

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"testing"
	"time"
)

var _testBacks = func(ctx context.Context) {
	done := make(chan struct{})
	go func() {
		// 你的业务逻辑，比如说这里我们模拟的是将本地缓存刷新到数据库里面
		// 这里我们简单的睡一段时间来模拟
		log.Printf("刷新缓存中……")
		time.Sleep(1 * time.Second)
		close(done)
	}()
	select {
	case <-ctx.Done():
		log.Printf("刷新缓存超时")
	case <-done:
		log.Printf("缓存被刷新到了 DB")
	}
}

func Test_serviceStopHandle_IsCloseAllService(t *testing.T) {
	closeMode := 1

	s1 := NewServer("business", "localhost:8080")
	s2 := NewServer("admin", "localhost:8081")
	app := NewApp([]*Server{s1, s2},
		SetShutdownCloseMode(closeMode),
		WithShutdownCallbacks(_testBacks))
	stopCh := make(chan struct{})
	go func() {
		app.StartAndServe()
		close(stopCh)
	}()
	go func() {
		time.Sleep(1 * time.Second)
		app.SubmitCloseEvent(func(resource *closeResource) {
			// 模拟服务器出问题
			resource.errList = append(resource.errList, errors.New("模拟服务器关闭"))
		})
	}()
	<-stopCh
}

func Test_timeoutStopHandle_IsCloseAllService(t *testing.T) {
	closeMode := 2

	s1 := NewServer("business", "localhost:8080")
	s2 := NewServer("admin", "localhost:8081")
	app := NewApp([]*Server{s1, s2},
		SetShutdownCloseMode(closeMode),
		WithShutdownCallbacks(_testBacks))
	stopCh := make(chan struct{})
	go func() {
		app.StartAndServe()
		close(stopCh)
	}()
	go func() {
		time.Sleep(1 * time.Second)
		app.SubmitCloseEvent(func(resource *closeResource) {
			// 模拟ctrl+c 超时
			resource.signalRecord[os.Interrupt]++
		})
	}()
	<-stopCh
}

func Test_mandatoryStopHandle_IsCloseAllService(t *testing.T) {
	closeMode := 4

	s1 := NewServer("business", "localhost:8080")
	s2 := NewServer("admin", "localhost:8081")
	app := NewApp([]*Server{s1, s2},
		SetShutdownCloseMode(closeMode),
		WithShutdownCallbacks(_testBacks))
	stopCh := make(chan struct{})
	go func() {
		app.StartAndServe()
		close(stopCh)
	}()
	go func() {
		time.Sleep(1 * time.Second)
		app.SubmitCloseEvent(func(resource *closeResource) {
			// 模拟ctrl+c
			resource.signalRecord[os.Interrupt]++
			fmt.Println("第一次按ctrl+c")
		})
		time.Sleep(2 * time.Second)
		app.SubmitCloseEvent(func(resource *closeResource) {
			// 模拟ctrl+c
			resource.signalRecord[os.Interrupt]++
			fmt.Println("第二次按ctrl+c")
		})
	}()
	<-stopCh
}
