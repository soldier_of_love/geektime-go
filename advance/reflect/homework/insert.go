package homework

import (
	"database/sql"
	"errors"
	"fmt"
	"reflect"
	"strings"
)

var errInvalidEntity = errors.New("invalid entity")

// InsertStmt 作业里面我们这个只是生成 SQL，所以在处理 sql.NullString 之类的接口
// 只需要判断有没有实现 driver.Valuer 就可以了
func InsertStmt(entity interface{}) (string, []interface{}, error) {
	if entity == nil {
		return "", nil, errInvalidEntity
	}
	tableName := reflect.TypeOf(entity).Name()
	typ := reflect.TypeOf(entity)
	if typ.Kind() == reflect.Ptr {
		tableName = typ.Elem().Name()
	}
	if tableName == "" {
		return "", nil, errInvalidEntity
	}
	names := make([]string, 0)
	values := make([]any, 0)
	iterateField(entity, func(fieldName string, value any) {
		names = append(names, fieldName)
		values = append(values, value)
	})
	if len(names) == 0 {
		return "", nil, errInvalidEntity
	}
	cmd, filterValues := insertCmd(tableName, names, values)
	fmt.Println("cmd", cmd)
	return cmd, filterValues, nil
}

//homework.BaseEntity{CreateTime:123, UpdateTime:(*int64)(0x1400011ed78)}
//0x315
//sql.NullString{String:"Tom", Valid:true}
//(*sql.NullInt32)(nil)

func insertCmd(tableName string, names []string, values []any) (string, []any) {
	bd := new(strings.Builder)
	bd.WriteString(fmt.Sprintf("INSERT INTO `%s`", tableName))

	filter := map[string]struct{}{}
	// todo 不合理过滤
	filterValues := make([]any, 0, len(values))
	filterNames := make([]string, 0, len(names))
	for i := 0; i < len(names); i++ {
		v := names[i]
		if _, ok := filter[v]; ok {
			continue
		}
		filter[v] = struct{}{}
		filterValues = append(filterValues, values[i])
		filterNames = append(filterNames, fmt.Sprintf("`%s`", v))
	}
	bd.WriteString("(")
	bd.WriteString(strings.Join(filterNames, ","))
	bd.WriteString(")")
	bd.WriteString(" VALUES(")

	fields := make([]string, 0, len(filterNames))
	for i := 0; i < len(filterNames); i++ {
		fields = append(fields, "?")
	}
	bd.WriteString(strings.Join(fields, ","))
	bd.WriteString(");")
	return bd.String(), filterValues
}

func availableEntity(entity any) (any, bool) {
	for reflect.TypeOf(entity).Kind() == reflect.Ptr {
		v := reflect.ValueOf(entity)
		if v.IsNil() {
			return nil, false
		}
		entity = reflect.Indirect(v).Interface()
	}
	return entity, true
}

func iterateField(entity any, f func(fieldName string, value any)) {
	if entity == nil {
		return
	}
	obj, ok := availableEntity(entity)
	if !ok {
		return
	}
	v := reflect.ValueOf(obj)
	for i := 0; i < v.NumField(); i++ {
		var (
			field = v.Field(i)
			kind  = field.Kind()
		)
		name := v.Type().Field(i).Name
		switch kind {
		case reflect.Struct:
			tmp := field.Interface()
			if v, ok := tmp.(sql.NullString); ok {
				f(name, v)
				continue
			} else if v, ok := tmp.(*sql.NullInt32); ok {
				f(name, v)
				continue
			}
			if reflect.DeepEqual(tmp, reflect.Zero(field.Type()).Interface()) {
				f(name, tmp)
				continue
			}
			iterateField(tmp, f)
		case reflect.Slice, reflect.Array:
			iterateField(field.Interface(), func(fieldName string, value any) {
				f(fmt.Sprintf("%s.%s", name, fieldName), value)
			})
		default:
			f(name, getValue(field))
		}
	}
}

func getValue(v reflect.Value) (res any) {
	switch v.Kind() {
	case reflect.String:
		res = v.String()
	case reflect.Float32, reflect.Float64:
		res = v.Float()
	case reflect.Bool:
		res = v.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		res = v.Int()
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		res = v.Uint()
	default:
		if v.CanInterface() {
			res = v.Interface()
		}
	}
	return
}
