package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"gitee.com/geektime-geekbang/geektime-go/advance/template/gen/annotation"
	"gitee.com/geektime-geekbang/geektime-go/advance/template/gen/http"
	"go/ast"
	"go/parser"
	"go/token"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"
	"unicode"
)

// 实际上 main 函数这里要考虑接收参数
// src 源目标
// dst 目标目录
// type src 里面可能有很多类型，那么用户可能需要指定具体的类型
// 这里我们简化操作，只读取当前目录下的数据，并且扫描下面的所有源文件，然后生成代码
// 在当前目录下运行 go install 就将 main 安装成功了，
// 可以在命令行中运行 gen
// 在 testdata 里面运行 gen，则会生成能够通过所有测试的代码
func main() {
	err := gen(".")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("success")
}

func gen(src string) error {
	// 第一步找出符合条件的文件
	srcFiles, err := scanFiles(src)
	if err != nil {
		return err
	}
	// 第二步，AST 解析源代码文件，拿到 service definition 定义
	defs, err := parseFiles(srcFiles)
	if err != nil {
		return err
	}
	// 生成代码
	return genFiles(src, defs)
}

// 根据 defs 来生成代码
// src 是源代码所在目录，在测试里面它是 ./testdata
func genFiles(src string, defs []http.ServiceDefinition) error {
	for _, def := range defs {
		bs := &bytes.Buffer{}
		err := http.Gen(bs, def)
		name := fmt.Sprintf("%s/%s_gen.go", src, underscoreName(def.Name))
		file, err := os.OpenFile(name, os.O_CREATE|os.O_WRONLY, os.ModePerm)
		if err != nil {
			return err
		}
		if err != nil {
			return err
		}
		writer := bufio.NewWriter(file)
		writer.Write(bs.Bytes())
		writer.Flush()
		file.Close()
	}
	return nil
}

func parseFiles(srcFiles []string) ([]http.ServiceDefinition, error) {
	defs := make([]http.ServiceDefinition, 0, 20)
	for _, src := range srcFiles {
		// 你需要利用 annotation 里面的东西来扫描 src，然后生成 file
		file := parseFile(src)
		for _, typ := range file.Types {
			_, ok := typ.Annotations.Get("HttpClient")
			if !ok {
				continue
			}
			def, err := parseServiceDefinition(file.Node.Name.Name, typ)
			if err != nil {
				return nil, err
			}
			defs = append(defs, def)
		}
	}
	return defs, nil
}

func parseFile(fileName string) annotation.File {
	tmp := token.NewFileSet()
	f, err := parser.ParseFile(tmp, fileName, nil, parser.ParseComments)
	if err != nil {
		log.Fatalln(err)
	}
	tv := new(annotation.SingleFileEntryVisitor)
	ast.Walk(tv, f)
	return tv.Get()
}

// 你需要利用 typ 来构造一个 http.ServiceDefinition
// 注意你可能需要检测用户的定义是否符合你的预期
func parseServiceDefinition(pkg string, typ annotation.Type) (http.ServiceDefinition, error) {
	service := http.ServiceDefinition{
		Package: pkg,
		Name:    typ.Annotations.Node.Name.Name,
		Methods: nil,
	}
	if v, ok := typ.Annotations.Get("ServiceName"); ok {
		service.Name = v.Value
	}
	for _, field := range typ.Fields {
		if len(field.Annotations.Node.Names) == 0 {
			continue
		}
		name := field.Annotations.Node.Names[0].Name
		tmp := http.ServiceMethod{
			Name:         name,
			Path:         fmt.Sprintf("/%s", name),
			ReqTypeName:  "",
			RespTypeName: "",
		}
		if v, ok := field.Annotations.Get("Path"); ok {
			tmp.Path = v.Value
			fmt.Println("--111--", v.Value)
		}
		// 参数方法第一个ctx 第二个request
		if v, ok := field.Annotations.Node.Type.(*ast.FuncType); ok && len(v.Params.List) == 2 {
			if req, ok := v.Params.List[1].Type.(*ast.StarExpr).X.(*ast.Ident); ok {
				tmp.ReqTypeName = req.Name
			}
		} else {
			return service, errors.New("gen: 方法必须接收两个参数，其中第一个参数是 context.Context，第二个参数请求")
		}
		// 返回值字段 第一个response 第二个error
		if v, ok := field.Annotations.Node.Type.(*ast.FuncType); ok && len(v.Results.List) == 2 {
			if res, ok := v.Results.List[0].Type.(*ast.StarExpr).X.(*ast.Ident); ok {
				tmp.RespTypeName = res.Name
			}
		} else {
			return service, errors.New("gen: 方法必须返回两个参数，其中第一个返回值是响应，第二个返回值是error")
		}
		service.Methods = append(service.Methods, tmp)
	}
	return service, nil
}

// 返回符合条件的 Go 源代码文件，也就是你要用 AST 来分析这些文件的代码
func scanFiles(src string) ([]string, error) {
	if len(src) == 0 {
		return nil, errors.New("path is not empty")
	}
	files := make([]string, 0)
	err := filepath.Walk(src, func(path string, fn fs.FileInfo, err error) error {
		if strings.HasSuffix(path, ".go") && !strings.HasSuffix(path, "test_go") {
			files = append(files, path)
		}
		return nil
	})
	// 没有注解的去掉
	filterFiles := make([]string, 0)
	signs := []string{"@HttpClient"}
	for _, path := range files {
		data, err := os.ReadFile(path)
		if err != nil {
			return nil, err
		}
		content := string(data)
		for _, v := range signs {
			if strings.Index(content, v) > 0 {
				filterFiles = append(filterFiles, path)
				break
			}
		}
	}
	if len(filterFiles) == 0 {
		return nil, errors.New(fmt.Sprintf("file is not empty"))
	}
	return filterFiles, err
}

// underscoreName 驼峰转字符串命名，在决定生成的文件名的时候需要这个方法
// 可以用正则表达式，然而我写不出来，我是正则渣
func underscoreName(name string) string {
	var buf []byte
	for i, v := range name {
		if unicode.IsUpper(v) {
			if i != 0 {
				buf = append(buf, '_')
			}
			buf = append(buf, byte(unicode.ToLower(v)))
		} else {
			buf = append(buf, byte(v))
		}

	}
	return string(buf)
}
